# noinspection PyUnresolvedReferences
from conans import ConanFile

class JsonConan(ConanFile):
  name = "json"
  version = "1.0"
  license = "MIT"
  author = "Sergey A. Erkin (sergey.a.erkin@gmail.com)"
  url = "https://saerkin@bitbucket.org/saerkin/cpp-json.git"
  settings = "os", "compiler", "build_type", "arch"
  exports_sources = "include/*"
  no_copy_source = True
  generators = "cmake_multi"
  requires = \
    "lifescope/1.0@sergey-erkin/stable"
  build_requires = \
    "Catch/1.12.1@bincrafters/stable"

  def package(self):
    self.copy("*.h")

  def package_id(self):
    self.info.header_only()
