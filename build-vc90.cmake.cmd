@mkdir build-vc90
@pushd build-vc90
cmake build-vc90 -G "Visual Studio 9 2008" ..\
cmake --build . --config Release --target ALL_BUILD
ctest -C Release
@popd