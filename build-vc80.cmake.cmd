@mkdir build-vc80
@pushd build-vc80
cmake build-vc80 -G "Visual Studio 8 2005" ..\
cmake --build . --config Release --target ALL_BUILD
ctest -C Release
@popd