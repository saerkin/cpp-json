#define CATCH_CONFIG_MAIN
#include <catch.hpp>
#include <json.h>

TEST_CASE("JsonPrimitive can be constructed from bool", "[json-primitive]") {
  LifeScope lifeScope;
  REQUIRE((new JsonPrimitive(lifeScope, true))->asBool());
  REQUIRE(!(new JsonPrimitive(lifeScope, false))->asBool());
}

TEST_CASE("JsonPrimitive can be constructed from string", "[json-primitive]") {
  LifeScope lifeScope;
  REQUIRE((new JsonPrimitive(lifeScope, "a string"))->asString() == "a string");
  REQUIRE((new JsonPrimitive(lifeScope, ""))->asString() == "");
}

TEST_CASE("JsonPrimitive can be printed to json", "[json-primitive]") {
  LifeScope lifeScope;
  REQUIRE((new JsonPrimitive(lifeScope, "a string"))->toString() == "\"a string\"");
  REQUIRE((new JsonPrimitive(lifeScope, ""))->toString() == "\"\"");
  REQUIRE((new JsonPrimitive(lifeScope, true))->toString() == "true");
  REQUIRE((new JsonPrimitive(lifeScope, false))->toString() == "false");
}
