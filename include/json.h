/******************************************************************************/
/* MIT License                                                                */
/*                                                                            */
/* Copyright (c) 2019 Sergey A Erkin                                          */
/*                                                                            */
/* Permission is hereby granted, free of charge, to any person obtaining a    */
/* copy of this software and associated documentation files (the "Software"), */
/* to deal in the Software without restriction, including without limitation  */
/* the rights to use, copy, modify, merge, publish, distribute, sublicense,   */
/* and/or sell copies of the Software, and to permit persons to whom          */
/* the Software is furnished to do so, subject to the following conditions:   */
/*                                                                            */
/* The above copyright notice and this permission notice shall be included    */
/* in all copies or substantial portions of the Software.                     */
/*                                                                            */
/* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR */
/* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,   */
/* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL    */
/* THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER */
/* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING    */
/* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER        */
/* DEALINGS IN THE SOFTWARE.                                                  */
/******************************************************************************/

#pragma once

#include <string>
#include <cstddef>
#include <stdexcept>
#include <lifescope.h>

struct Json;
struct JsonArray;
struct JsonObject;
class JsonPrimitive;

class JsonMutantArray;
class JsonMutantObject;

/**
 *
 */
enum JsonType {
  JSON_ARRAY,
  JSON_OBJECT,
  JSON_PRIMITIVE
};

/**
 * Root interface for json primitives, arrays and objects
 */
struct Json : public LifeScopeObject {
  public:
    virtual JsonType type() const = 0;

    virtual std::string toString() const = 0;

    /**
     * Narrow to json array
     * @throws std::domain_error if this json is not a json array
     */
    virtual JsonArray* asJsonArray() const = 0;

    /**
     * Narrow to json object
     * @throws std::domain_error if this json is not a json object
     */
    virtual JsonObject* asJsonObject() const = 0;

    /**
     * Narrow to json primitive
     * @throws std::domain_error if this json is not a json primitive
     */
    virtual JsonPrimitive* asJsonPrimitive() const = 0;

  protected:
    virtual ~Json() {}
};

/**
 * An immutable son primitive: a bool, a string or a number
 * todo: #11 Support number
 */
class JsonPrimitive : public Json {
  private:
    const bool* pBooleanValue;
    const std::string* pStringValue;

  public:
    JsonPrimitive(LifeScope& lifeScope, bool value);

    JsonPrimitive(LifeScope& lifeScope, const std::string& value);

    JsonPrimitive(LifeScope& lifeScope, const char* value);

  public:
    JsonType type() const;

    /**
     * Convert to string
     */
    std::string toString() const;

    /**
     * Cast to boolean
     * @throws std::domain_error if this primitive cannot be casted to boolean
     */
    bool asBool() const;

    /**
     * Cast to string
     * @throws std::domain_error if this primitive cannot be casted to string
     */
    std::string asString() const;

  private:
    JsonArray* asJsonArray() const;

    JsonObject* asJsonObject() const;

    JsonPrimitive* asJsonPrimitive() const;

  private:
    ~JsonPrimitive();
};

/**
 * An immutable json array
 */
struct JsonArray : public Json {
  public:
    /**
     * @return length of this array
     */
    virtual std::size_t size() const = 0;

    /**
     * @return an array element by its index
     * @throws std::out_of_range
     */
    virtual Json* get(std::size_t index) const = 0;

  protected:
    virtual ~JsonArray() {}
};

/**
 * An immutable json object
 */
struct JsonObject : public Json {
  public:
    /**
     * @return count of this object members
     */
    virtual std::size_t size() const = 0;

    /**
     * @return an object member by its index
     * @throws std::out_of_range
     */
    virtual Json* get(std::size_t index) const = 0;

    /**
     * @return a member by its name or null if no member is found
     */
    virtual Json* get(const char* name) const = 0;

    /**
     * @return true if this object contains a member with specified name
     */
    virtual bool has(const char* name) const = 0;

  protected:
    virtual ~JsonObject() {}
};

/**
 * A mutable object to construct json array
 */
class JsonMutantArray : private JsonArray {
  public:
    /**
     * Append an element to the end of array and return this array
     */
    JsonMutantArray* add(Json* element);

    JsonMutantArray* add(bool element);

    JsonMutantArray* add(const std::string& element);

    /**
     * Finish construction and return final json array
     */
    JsonArray* freeze();
};

/**
 * A mutable object to construct json object
 */
class JsonMutantObject : private JsonObject {
  public:
    /**
     * Add new member to the object and return this object
     * @throws std::invalid_argument if this object has another member with same name
     * @throws std::domain_error if this object is frozen
     */
    JsonMutantObject* put(const std::string& name, Json* member);

    JsonMutantObject* put(const std::string& name, bool member);

    JsonMutantObject* put(const std::string& name, const std::string& member);

    /**
     * Finish construction and return final json object
     */
    JsonObject* freeze();
};