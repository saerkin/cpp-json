#include "precomp.h"
#include <json.h>

using namespace std;

JsonPrimitive::JsonPrimitive(LifeScope& lifeScope, bool value) {
  lifeScope.put(this);
  pBooleanValue = new bool(value);
  pStringValue = NULL;
}

JsonPrimitive::JsonPrimitive(LifeScope& lifeScope, const string& value) {
  lifeScope.put(this);
  pBooleanValue = NULL;
  pStringValue = new string(value);
}

JsonPrimitive::JsonPrimitive(LifeScope& lifeScope, const char* value) {
  lifeScope.put(this);
  pBooleanValue = NULL;
  pStringValue = new string(value);
}

JsonType JsonPrimitive::type() const {
  return JSON_PRIMITIVE;
}

JsonPrimitive* JsonPrimitive::asJsonPrimitive() const {
  return const_cast<JsonPrimitive*>(this);
}

JsonObject* JsonPrimitive::asJsonObject() const {
  throw domain_error("Cannot cast json primitive to an object");
}

JsonArray* JsonPrimitive::asJsonArray() const {
  throw domain_error("Cannot cast json primitive to an array");
}

string JsonPrimitive::asString() const {
  if (pStringValue != NULL)
    return *pStringValue;
  if (pBooleanValue != NULL)
    return *pBooleanValue ? "true" : "false";
  return "";
}

bool JsonPrimitive::asBool() const {
  if (pBooleanValue != NULL)
    return *pBooleanValue;
  throw domain_error("Cannot cast json primitive to boolean");
}

string JsonPrimitive::toString() const {
  if (pStringValue != NULL)
    return '"' + *pStringValue + '"';
  if (pBooleanValue != NULL)
    return *pBooleanValue ? "true" : "false";
  return "";
}

JsonPrimitive::~JsonPrimitive() {
  if (pBooleanValue != NULL)
    delete pBooleanValue;
  if (pStringValue != NULL)
    delete pStringValue;
}
